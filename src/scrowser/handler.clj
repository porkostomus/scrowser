(ns scrowser.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]])
  (:use [ring.middleware.params]
        [ring.util.response]
        [ring.adapter.jetty]))

(defn raw [name] (str "<xmp>" (slurp name) "</xmp>"))

(defn page [name]
  (str "<html><head>
    <meta charset='UTF-8'>
    <title>Source Code Browser</title>
    <link rel='stylesheet' type='text/css' href='/css/style.css'>
  </head><body><div>"
       (if name
         (str "<form>"
              "Name: <input name='name' type='text'>"
              "<input type='submit'>"
              "</form><br>"(raw (str "https://" name)))
         (str "<form>"
              "Name: <input name='name' type='text'>"
              "<input type='submit'>"
              "</form>"))
       "</div></body></html>"))

(defn handler [{{name "name"} :params}]
  (-> (response (page name))
      (content-type "text/html")))

(def app
  (-> handler wrap-params))
